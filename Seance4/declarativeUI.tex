% ©2014, Sylvain TENIER - Esigelec, Rouen, France
% Licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
% https://creativecommons.org/licenses/by-nc-sa/4.0/
\documentclass[svgnames,hyperref={pdfpagelabels=false}]{beamer}
\let\Tiny=\tiny %suppression d'un warning beamer
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{multicol}
\usepackage[round]{natbib}
\usetheme{Warsaw}


\title{JAVA FX et FXML : interfaces déclaratives pour JAVA}
\author[s.tenier@groupe-esigelec.com]{Sylvain Tenier - D\'epartement TIC - Esigelec}
\date{mardi 12 mai 2015}

\newcommand {\framedgraphic}[3] {
\begin{frame}{#1}
  \begin{figure}
	\begin{center}
	  \includegraphics[width=\textwidth,height=0.75\textheight,keepaspectratio]{#2}\caption{#3}
	\end{center}
\end{figure}
\end{frame}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Plan}
    \tableofcontents[currentsection]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Plan}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

\begin{document}

\frame{\titlepage}

\section{Motivations}

\begin{frame}
\frametitle{Objectifs}
Selon \cite{conf/dsvis/Silva00}, les interfaces déclaratives offrent 3 avantages :
\begin{enumerate}
	\item Description de l'interface en dehors du contexte du programme
	\item Infrastructure pour l'automatisation du design et de l'implémentation
	\item Modifications et réutilisations facilitées
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Qu'est-ce que JAVAFX ?}
\begin{itemize}
	\item Nouvelle génération de RCP (Rich Client Platform) pour JAVA
	\begin{itemize}
		\item Client ``officiel'' Oracle
		\item Swing est désormais en mode ``maintenance''
	\end{itemize}
	\item Conçu pour les besoins des applications d'entreprises
	\begin{itemize}
		\item légéreté
		\item support de l'accélération matérielle
		\item intégration native de contenu Web 
	\end{itemize}
	\item Présent en standard dans la JVM (depuis la version 7u6)
	\begin{itemize}
		\item Utilisation depuis les autres langages supportés par la JVM (JRuby, Jython, Groovy, Scala...)
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Java FX et le modèle MVC}
Une application Java FX peut être décomposée suivant le modèle MVC
\begin{enumerate}
\item Les \emph{Modèles} sont définis dans des classes JAVA
\item Chaque \emph{Vue} est définie \emph{en XML} dans un fichier .fxml
\item Le \emph{Contrôleur} de chaque vue est une classe JAVA
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{La métaphore théâtrale}
Les étapes de création sont les suivantes :
\begin{enumerate}
	\item Création de la \emph{piste} (classe \texttt{Stage})
	\begin{itemize}
		\item dépend de la plateforme de déploiement (tablette, ordinateur, page web)
	\end{itemize}
	\item Création de la \emph{scène} (classe \texttt{Scene})
	\begin{itemize}
		\item permet l'interaction entre les acteurs internes et avec les utilisateurs 
	\end{itemize}
	\item Création des noeuds (sous-classes de la classe \texttt{Node})
	\begin{itemize}
		\item \'Elements finaux (formes, textes, images...)
		\item Conteneurs d'éléments (gestion de groupes de noeuds)
	\end{itemize}
	\item Création des modèles et synchronisation (\emph{binding}) entre les noeuds et le modèle
	\item Création des évènements et des animations
\end{enumerate}
\end{frame}

\section{FXML appliqué}

\framedgraphic{Application ``Tirage aléatoire''}{appli.PNG}{Fenêtre principale après un clic} 

\begin{frame}[fragile]
\frametitle{Préparation de la classe principale}
\lstset{
	language=JAVA,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{enumerate}
\item Configurer Eclipse avec un JDK > 7u6
\item JDK 7 : ajouter jfxrt.jar au projet (fourni avec le JDK)
\item Importer les classes du package fxml :
	\begin{lstlisting}
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
	\end{lstlisting}
\end{enumerate}
\end{frame}

\begin{frame}[fragile]
\frametitle{Définition de la classe principale}
\lstset{
	language=JAVA,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{itemize}
\item Appel de la méthode launch de la classe Application
\item Chargement du FXML dans le noeud racine
\item Création des éléments (Stage, Scene) et affichage

\end{itemize}

\begin{lstlisting}
public class Aleatoire extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Tirage aleatoire");
        Parent root = FXMLLoader.load(getClass().getResource("gui.fxml"));
        stage.setScene(new Scene(root));
        stage.show();
    }
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Fichier .fxml (1/2) }
\lstset{
  language=XML,
  basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
  breaklines=true
}

\begin{enumerate}
  \item D\'eclaration XML
  \item Imports de classes FXML (Processing Instructions XML)
  \item Balisage XML (page 2)
\end{enumerate}


\begin{lstlisting}
<?xml version="1.0" encoding="UTF-8"?>

<?import java.lang.*?>
<?import java.net.*?>
<?import javafx.collections.*?>
<?import javafx.scene.control.*?>
<?import javafx.scene.layout.*?>

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Fichier .fxml (2/2) }
\lstset{
  language=XML,
  basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
  breaklines=true
}


\begin{lstlisting}
<GridPane id="Panel" hgap="10" vgap="10" xmlns:fx="http://javafx.com/javafx" fx:controller="aleatoire.Controller">
  <columnConstraints>
    <ColumnConstraints minWidth="60" halignment="right"/>
    <ColumnConstraints prefWidth="300" hgrow="always"/>
   </columnConstraints>
  <children>
    <Slider fx:id="slider" GridPane.rowIndex="1" GridPane.columnIndex="1" majorTickUnit="1" max="7" min="5" minorTickCount="0" showTickLabels="true" showTickMarks="true" snapToTicks="true" />
    <Button fx:id="button" GridPane.rowIndex="1" GridPane.columnIndex="2" onAction="#handleButtonAction" text="Tirage" />
    <Label fx:id="label"  GridPane.rowIndex="2" GridPane.halignment="center" GridPane.columnSpan="2" />
  </children>
</GridPane>
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Classe Contr\^oleur (1/2) }
\lstset{
  language=JAVA,
  basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
  breaklines=true
}
 
 
\begin{enumerate}
  \item Le nom de la classe correspond \`a l'attribut \texttt{fx:controller} de la racine du fichier .fxml
  \item La classe doit impl\'ementer l'interface Initializable
\end{enumerate}

\begin{lstlisting}
package aleatoire;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Classe Contr\^oleur (2/2) }
\lstset{
  language=JAVA,
  basicstyle=\ttfamily\scriptsize,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
  breaklines=true
}
 
\begin{lstlisting}
public class Controller implements Initializable {
    @FXML
    private Label label;
    @FXML
    private Slider slider;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        Random r =new Random();
        int tirage=r.nextInt((int)slider.getValue())+1;
        label.setText("Chiffre aleatoire : "+ tirage);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //pas d'initialisation necessaire
    }    
}
\end{lstlisting}
\end{frame}



\section{Support de FXML}

\begin{frame}
\frametitle{Javadoc et FXML}
\begin{itemize}
	\item Pas de documentation spécifique à FXML
	\item Traduire depuis la javadoc
	\begin{enumerate}
		\item Le nom de l'\'el\'ement correspond \`a la classe
		\item Les attributs correspondent aux méthodes et attributs
	\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Outils de développements}
\begin{itemize}
	\item Versions récentes des principaux IDE
	\begin{enumerate}
		\item Netbeans > 7.3 
		\item Plugin e(fx)clipse à partir de Eclipse Kepler
		\item Intellij Idea > 12.1 
	\end{enumerate}
	\item Complétion de code FXML
	\item \'Editeur graphique
\end{itemize}
\end{frame}


\begin{frame}{Références}
    \bibliographystyle{plainnat}
    \bibliography{seance4}
\end{frame}

\end{document}
