S�ance 1 - partie 3 (namespaces)

- montrer les scopes des namespaces
- expliciter la cr�ation des URI (partie d�sambiguiation/partie nommage)
- �tre plus explicite sur la notion d'espace de nom par d�faut
- montrer un exemple
- bien pr�ciser qu'un espace de nom repr�sente un ensemble d'�l�ments d'une th�matique. Il n'y a pas un espace de nom par �l�ment.