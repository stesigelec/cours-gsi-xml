<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <head>
		<title>Le tableau du cluedo !</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="style.css" />
      </head>
      <body>
        <table>
        <tr id="nom"><td></td><xsl:apply-templates select="//noms" /></tr>
        <tr id="photo"><td></td><xsl:apply-templates select="cluedo/personnages/personnage/photo" /></tr>
		<xsl:apply-templates select="cluedo/armes/arme" />
        </table>
      </body>
    </html>
  </xsl:template>
    
  <xsl:template match="noms">
	<td><xsl:value-of select="./nom[@lang='fr']" /></td>
  </xsl:template>  

  <xsl:template match="photo">
	<td><img width="110px" alt="{.}" src="Photos/{./@url}" /></td>
  </xsl:template>  

    <xsl:template match="arme">
	<tr class="arme"><td><img width="55px" height="55px" alt="{.}" src="Photos/{./@url}" /></td>
	<xsl:for-each select="/cluedo/personnages/personnage">
	<td></td>
	</xsl:for-each>
	<!--<xsl:apply-templates select="/cluedo/personnages/personnage" />-->	
	</tr>
  </xsl:template>  
  
<xsl:template match="personnage">
  	<td>?</td>
</xsl:template>  

   
   </xsl:stylesheet>

