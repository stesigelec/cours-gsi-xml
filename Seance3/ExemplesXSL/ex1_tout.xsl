<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="5.0" />
	<xsl:template match="/">
		<h1>Contenu</h1>
		<p><xsl:apply-templates /></p>
		<p>_ _ _ _ _</p>
	</xsl:template>
</xsl:stylesheet>

