<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<html>
			<head>
				<title>Utilisation des modes</title>
			</head>
			<body>
				Affichage en gras : <xsl:apply-templates select=".//nom" mode="gras"/>
				<br/>
				Affichage en italiques<xsl:apply-templates select=".//nom" mode="italique"/>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="nom" mode="italique">
		<p style="font-style:italic;">
			<xsl:value-of select="."/>
		</p>
	</xsl:template>
	<xsl:template match="nom" mode="gras">
		<p style="font-weight:bolder;">
			<xsl:value-of select="."/>
		</p>
	</xsl:template>
</xsl:stylesheet>

