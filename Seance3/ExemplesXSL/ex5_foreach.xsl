<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="5.0" />
	<xsl:template match="/">
    <html>
      <head><title>Exemple 5</title></head>
      <body>
		<ul>
		<xsl:for-each select="//lyceen">
	  		<li><em><xsl:value-of select="./etatcivil/nom" /> </em>  <xsl:value-of select=".//prenom" /></li>
		</xsl:for-each>
		</ul>
	  </body>
    </html>
	</xsl:template>
</xsl:stylesheet>
