<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="5.0" />
	<xsl:template match="/">
    <html>
      <head><title>Affichage des lycéens</title></head>
      <body>		
        <h1>Lycéens</h1>
        <ul><xsl:apply-templates /></ul>
      </body>
    </html>
	</xsl:template>
	
	<xsl:template match="nom">
		<li><em><xsl:value-of select="." /></em></li>
	</xsl:template>  
</xsl:stylesheet>
