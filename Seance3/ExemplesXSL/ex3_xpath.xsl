<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="5.0" />
	<xsl:template match="/">
    <html>
      <head><title>Exemple 3</title></head>
      <body>		
        <table style="border:1px solid black;"><tr><th>Nom</th><th>Prenom</th><th>Annee</th></tr>
        <xsl:apply-templates select="//lyceen" /></table>
      </body>
    </html>
	</xsl:template>
	
	<xsl:template match="lyceen">
	<tr>
		<td>(chemin relatif) <xsl:value-of select="etatcivil/nom" /></td>
		<td>(descendant) <xsl:value-of select=".//prenom" /></td>
		<td>(attribut) <xsl:value-of select="scolarite/@annee" /></td>
	</tr>
	</xsl:template>  
</xsl:stylesheet>
