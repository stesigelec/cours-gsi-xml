<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="5.0" />
	<xsl:template match="/">
    <html>
      <head><title>Exemple 4</title></head>
      <body>
		<h1>Etats-civils :</h1><ul><xsl:apply-templates select="lyceens/lyceen/etatcivil" /></ul>
		<h1>Scolarités  :</h1><ul><xsl:apply-templates select="//scolarite" /></ul>
      </body>
    </html>
	</xsl:template>
	
	<xsl:template match="etatcivil">
		<li><em><xsl:value-of select="nom" /></em> <xsl:value-of select="prenom" /></li>
	</xsl:template>  
	
	<xsl:template match="scolarite">
		<li><xsl:value-of select="description" /></li>
	</xsl:template>  
	
</xsl:stylesheet>
